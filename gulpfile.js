'use strict';
const path = require('path');
const gulp = require('gulp');
const del = require('del');
const debug = require('gulp-debug');
const gulpif = require('gulp-if');
const concat = require('gulp-concat');
const rename = require('gulp-rename');
const sourcemaps = require('gulp-sourcemaps');
const inject = require('gulp-inject');
const uglify = require('gulp-uglify');
const jsonmin = require('gulp-json-minify');
const htmlmin = require('gulp-htmlmin');
const stylus = require('gulp-stylus');


const isProd = process.env.NODE_ENV === 'production';


/*
 * Set configuration values
 */
const config = {
	tmp: 'tmp',
	dest: 'bundle',

	components: {
		pattern: 'component-pattern.tpl',
		srcRoot: 'src/components',
		dest: 'bundle/components',
		tmpSubFolder: 'components',
		list: [
			'ui-element',
			'ui-window',
			'ui-i18n',
			'ui-button',
			'ui-toggle',
			'ui-drop-down',
			'ui-modal',
			'ui-range',
			'ui-settings-button',
			'ui-player',
			'ui-playlist',
			'ui-frequency-visualization',
			'ui-equalizer',
			'ui-settings'
		]
	},

	scripts: {
		src: ['src/scripts/lib/**/*.js', 'src/scripts/**/*.js'],
		name: 'bundle.min.js',
		dest: 'bundle/js'
	},

	styles: {
		src: 'src/styles/styles.styl',
		dest: 'bundle/styles'
	},

	icons: {
		src: 'src/icons/**/*.png',
		dest: 'bundle/icons'
	},

	fonts: {
		src: 'src/fonts/**/*.ttf',
		dest: 'bundle/fonts'
	},

	locales: {
		src: 'src/_locales/*/messages.json',
		dest: 'bundle/_locales'
	},

	manifest: {
		src: 'src/manifest.json',
		dest: 'bundle'
	},

	background: {
		src: 'src/background.js',
		dest: 'bundle'
	},


	index: {
		src: 'src/index.html',
		dest: 'bundle'
	}

};


config.destPrefixRe = new RegExp(`(\/)?${config.dest}(\/)?`);






/*
 *  COMPONENTS BUILDING
 *
 *  Registering handlers
 *  for each component of the piecemeal
 */

config.components.list.forEach(name => {

	let compPath = path.join(config.components.srcRoot, name);
	let tmpPath = path.join(config.tmp, config.components.tmpSubFolder, name);
	let destPath = path.join(config.components.dest, name);



	// concat (and uglify) component scripts files
	gulp.task(`build:components:${name}:scripts`, () => {
		return gulp.src(`${compPath}/**/*.js`, {allowEmpty: true})
			.pipe(gulpif(isProd, concat(name + '.min.js')))
			// .pipe(gulpif(isProd, uglify({mangle: true}))) //todo fix for es6 syntax
			.pipe(gulp.dest(destPath));
	});



	// copy (and minify) templates
	gulp.task(`build:components:${name}:templates`, () => {
		return gulp.src(`${compPath}/**/*.html`, {allowEmpty: true})
			.pipe(gulpif(isProd, htmlmin({collapseWhitespace: true}))) //todo test
			.pipe(gulp.dest(tmpPath));
	});



	// compile (and minify) component stylus styles
	gulp.task(`build:components:${name}:styles`, () => {
		return gulp.src(`${compPath}/${name}.styl`, {allowEmpty: true})
			.pipe(stylus({
				compress: !!isProd,
				define: {
					url: file => {
						let prefix = config.components.dest.replace(config.destPrefixRe, '/');
						let url = file.val.replace(/^\//, '');
						file.val = `url(${path.join(prefix, name, url)})`;
						file.quote = '';
						return file;
					}
				}
			}))
			.pipe(gulp.dest(tmpPath));
	});




	// copy component icons
	gulp.task(`build:components:${name}:icons`, () => {
		return gulp.src(`${compPath}/**/*.{svg,png,gif,jpeg,jpg}`, {allowEmpty: true})
			.pipe(gulp.dest(destPath));//TODO use svgmin
	});



	// concatenate together
	gulp.task(`build:components:${name}:concat`, () => {
		let templatesSource = gulp.src(`${tmpPath}/**/*.html`);
		let stylesSource = gulp.src(`${tmpPath}/**/*.css`);
		let scriptsSource = gulp.src(`${destPath}/**/*.js`);

		return gulp.src(config.components.pattern)
			.pipe(rename(path => {
				path.dirname = name;
				path.basename = name;
				path.extname = '.html';
			}))
			.pipe(inject(templatesSource, {
				removeTags: true,
				transform: (filePath, file) => file.contents.toString('utf8')
			}))
			.pipe(inject(stylesSource, {
				removeTags: true,
				transform:  (filePath, file) => `<style>\n${file.contents.toString('utf8')}\n</style>`
			}))
			.pipe(inject(scriptsSource, {
				removeTags: true,
				transform: (filePath, ...args) => inject.transform(filePath.replace(`/${config.dest}`, ''), ...args)
			}))
			.pipe(gulp.dest(config.components.dest));
	});


	// clear component temporary dir
	gulp.task(`clear-tmp:components:${name}`, () => {
		return del(tmpPath);
	});


	// component builder
	gulp.task(`build:components:${name}`, gulp.series(
		`clear-tmp:components:${name}`,
		gulp.parallel(
			`build:components:${name}:scripts`,
			`build:components:${name}:templates`,
			`build:components:${name}:styles`,
			`build:components:${name}:icons`
		),
		`build:components:${name}:concat`
	));

});



// Main build components task
gulp.task('build:components', gulp.parallel(
	...config.components.list.map(name => `build:components:${name}`))
);










/*
 *  SCRIPTS BUILDING
 *
 *  Build main page login scripts
 */
gulp.task('build:scripts', () => {
	return gulp.src(config.scripts.src)
		.pipe(gulpif(!isProd, sourcemaps.init()))
		.pipe(concat(config.scripts.name))
		// .pipe(gulpif(isProd, uglify({mangle: true}))) //todo fix for es6 syntax
		.pipe(gulpif(!isProd, sourcemaps.write()))
		.pipe(gulp.dest(config.scripts.dest));
});










/*
 *  STYLES OF APP BUILDING
 *
 *	Build stylus
 */
gulp.task('build:styles', () => {
	return gulp.src(config.styles.src)
		.pipe(stylus({
			compress: !!isProd
		}))
		.pipe(gulp.dest(config.styles.dest));
});










/*
 *  ICONS COPY
 *
 *	Just copy of application system icons
 */
gulp.task('build:icons', () => {
	return gulp.src(config.icons.src) //TODO use svgmin
		.pipe(gulp.dest(config.icons.dest));
});










/*
 *  FONTS COPY
 *
 *	Just copy of global fonts
 */
gulp.task('build:fonts', () => {
	return gulp.src(config.fonts.src)
		.pipe(gulp.dest(config.fonts.dest));
});










/*
 *  LOCALES
 *
 *  copy translation strings
 */
gulp.task('build:locales', () => {
	return gulp.src(config.locales.src)
		.pipe(gulpif(isProd, jsonmin())) //todo test
		.pipe(gulp.dest(config.locales.dest));
});









/*
 *  MANIFEST COPY
 *
 *  Copy (and compress) manifest.json
 */
gulp.task('build:manifest', () => {
	return gulp.src(config.manifest.src)
		.pipe(gulpif(isProd, jsonmin())) //todo test
		.pipe(gulp.dest(config.manifest.dest));
});










/*
 *  BACKGROUND
 *
 *  Copy (amd uglify) background.js script
 */
gulp.task('build:background', () => {
	return gulp.src(config.background.src)
		// .pipe(gulpif(isProd, uglify({mangle: true}))) //todo fix for es6 syntax
		.pipe(gulp.dest(config.background.dest));
});










/*
 *  INDEX PAGE BUILDING
 *
 *  Inject components and scripts into index
 *  adn distribute
 */
gulp.task('build:index', () => {
	let sources = gulp.src([path.join(config.scripts.dest, '*.js'), path.join(config.styles.dest, '**/*.css')]);
	let components = gulp.src(path.join(config.components.dest, '**/*.html'));

	return gulp.src(config.index.src)
		.pipe(inject(sources, {
			removeTags: true,
			transform: (filePath, ...args) => inject.transform(filePath.replace(config.destPrefixRe, '/'), ...args)
		}))
		.pipe(inject(components, {
			removeTags: true,
			starttag: '<!-- inject:components -->',
			transform: filePath => `<link rel="import" href="${filePath.replace(config.destPrefixRe, '/')}">`
		}))
		.pipe(gulp.dest(config.index.dest));
});










/*
 *  WATCHERS
 *
 *
 */

gulp.task('watch', function() {

	config.components.list.forEach(name => {
		let compPath = path.join(config.components.srcRoot, name);

		gulp.watch(`${compPath}/**/*.js`, gulp.series(`build:components:${name}:scripts`, `build:components:${name}:concat`));
		gulp.watch(`${compPath}/**/*.styl`, gulp.series(`build:components:${name}:styles`, `build:components:${name}:concat`));
		gulp.watch(`${compPath}/**/*.{svg,png,gif,jpeg,jpg}`, gulp.series(`build:components:${name}:icons`));
		gulp.watch(`${compPath}/**/*.html`, gulp.series(`build:components:${name}:templates`, `build:components:${name}:concat`));

	});

	gulp.watch(config.scripts.src, gulp.series('build:scripts'));

	gulp.watch(config.styles.src, gulp.series('build:styles'));

	gulp.watch(config.icons.src, gulp.series('build:icons'));

	gulp.watch(config.fonts.src, gulp.series('build:fonts'));

	gulp.watch(config.locales.src, gulp.series('build:locales'));

	gulp.watch(config.manifest.src, gulp.series('build:manifest'));

	gulp.watch(config.background.src, gulp.series('build:background'));

	gulp.watch(config.index.src, gulp.series('build:index'));

});











/*
 *  OTHER TASKS
 *
 *  clean tasks
 *  build together tasks
 *  default task
 *
 *
 */

gulp.task('clean', () => {
	return del(config.dest);
});


gulp.task('build', gulp.series(
	'clean',
	gulp.parallel(
		'build:components',
		'build:scripts',
		'build:styles',
		'build:icons',
		'build:fonts',
		'build:locales',
		'build:manifest',
		'build:background'
	),
	'build:index'
));

gulp.task('default', gulp.series('build', 'watch'));
