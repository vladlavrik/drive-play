'use strict';


chrome.app.runtime.onLaunched.addListener(() => {


	chrome.identity.getAuthToken({'interactive': true}, (token) => {

		if(!token) {

			//TODO answer, use cache data

			chrome.notifications.create('testMes', {
				type: 'basic',
				iconUrl: '/icons/icon48.png',
				title: chrome.i18n.getMessage('authErrorTitle'),
				message: chrome.i18n.getMessage('authErrorMsg')
			});

			return;
		}




		chrome.app.window.create('index.html', {
			id: 'player',
			frame: 'none',
			minWidth: 400,
			minHeight: 500,
			maxWidth: 600,
			width: 400,
			height: 600,
			left: 20,
			top: 20
		});


	});

});
