(scope => {
	'use strict';

	class EventEmitter {
		constructor() {
			this.__events = {};
		}

		__addListener(eventName, handler, ctx, useOnce, relatedBy) {

		}

		__removeListener(eventName, handler, ctx, useOnce, relatedBy) {

		}

		__triggerEvent(eventName, eventData) {

		}


		/*
		 * external methods
		 */

		/**
		 *
		 * @param eventName
		 * @param handler
		 * @param ctx
		 * @returns {*}
		 */
		on(eventName, handler, ctx) {
			return this.__addListener(eventName, handler, ctx, false);
		}


		/**
		 *
		 * @param eventName
		 * @param handler
		 * @param ctx
		 * @returns {*}
		 */
		once(eventName, handler, ctx) {
			return this.__addListener(eventName, handler, ctx, true);
		}


		/**
		 *
		 * @param eventName
		 * @param handler
		 * @param ctx
		 */
		onceOf(eventName, handler, ctx) { //todo future
		}


		/**
		 *
		 * @param eventName
		 * @param handler
		 * @returns {*}
		 */
		off(eventName, handler) {
			return this.__removeListener(eventName, handler);
		}


		/**
		 *
		 * @param eventName
		 * @param eventData
		 * @returns {*}
		 */
		trigger(eventName, eventData) {
			return this.__triggerEvent(eventName, eventData);

		}

	}


})(window.app = window.app || {});
