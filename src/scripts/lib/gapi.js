(scope => {
	'use strict';

	let SCOPE = 'https://www.googleapis.com/drive/v3/';
	let accessToken;



	function makeRequest(apiMethod, {method = 'GET', headers = {}, queryParams = {}}) {

		let queryString = Object.keys(queryParams)
			.map(key => `${key}=${queryParams[key]}`)
			.join('&');


		let url = SCOPE + apiMethod + (queryString ? '?' + queryString : '');

		// add auth access header
		headers['Authorization'] = 'Bearer ' + accessToken;

		return fetch(url, {method, headers})
			.then(response => {
				if(response.status == 200) {
					return response.json();
				}
				if(response.status == 401) {
					//TODO TOKEN REMOVE FROM CACHE AND FIRE LOGOUT EVENT
				}
				//TODO HANDLE ERROR

			});
	}



	scope.gApi = {



		/**
		 * @return Promise
		 */
		getToken() {
			return new Promise((resolve, reject) => {
				chrome.identity.getAuthToken({'interactive': true}, (token) => {
					if (token) {
						accessToken = token;
						resolve(token);
					} else {
						reject();
					}
				});
			})
		},


		/**
		 * @return Promise
		 */
		getUserInfo() {
			return new Promise((resolve, reject) => {
				chrome.identity.getProfileUserInfo(info => {
					if (info) {
						resolve(info);
					} else {
						reject();
					}
				});
			});
		},



		/**
		 * @return Promise
		 */
		// TODO do it
		removeTokenFromCache() {
			new Promise((resolve, reject) => {
			})
		},


		/**
		 * @return Promise
		 */
		getPlaylist({pageSize = 1000} = {}) {
			return makeRequest('files', {
				queryParams: {
					pageSize,
					q: 'mimeType=\'audio/mp3\''
					//TODO other audio formats
				}
			}).then(response => response.files);
		}


	};


})(window.app = window.app || {});
