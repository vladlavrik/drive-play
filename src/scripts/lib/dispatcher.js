(scope => {
	'use strict';

	class Dispatcher {
		constructor () {
			this.__actions = [];
		}


		/**
		 *
		 * @param {Function} callback
		 */
		register(callback) {
			this.__actions.push(callback);
		}


		unregister() {

		}


		/**
		 *
		 * @param {Object|Promise} action
		 * @param {string} [action.actionName]
		 */
		dispatch(action) {
			let actions = this.__actions;
			let results = actions.map(callback =>
				callback(action)
			);
			let resPromises = this._onlyPromise(results);

			if(resPromises.length) {
				return Promise.all(resPromises);
			}
			return Promise.resolve();
		}


		waitFor() {

		}

		_onlyPromise (arr) {
			return arr.filter(item => item instanceof Promise);
		}

	}


	scope.Dispatcher = Dispatcher;

})(window.app = window.app || {});
