(scope => {
	'use strict';

	scope.Storage = {

		/**
		 * @param {string|Array|Object} key
		 * @param {boolean} [useSync=false]
		 * @return Promise
         */
		get: (key, useSync) => new Promise((resolve, reject) => {
			let storeName = useSync ? 'sync' : 'local';

			chrome.storage[storeName].get(key, data => {
				if(data) {
					resolve(data);
				} else {
					reject(data);
				}
			});
		}),


		/**
		 * @param {Object} data
		 * @param {boolean} [useSync=false]
		 * @return Promise
		 */
		set: (data, useSync) => new Promise((resolve, reject) => {
			let storeName = useSync ? 'sync' : 'local';

			chrome.storage[storeName].set(data, () => {
				if(!chrome.runtime.lastError) { // TODO test it
					resolve();
				} else {
					reject(chrome.runtime.lastError);
				}
			});
		}),


		// TODO do it
		remove: (key, value, useSync) => {},


		// TODO do it
		clean: (key, value, useSync) => {}

	};


})(window.app = window.app || {});
