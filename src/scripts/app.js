(scope => {
    'use strict';


	//TODO button alwaysOnTop in window buttons

    // const components = {
    //     windowHeader: document.querySelector('window-header'),
    //     playerDisplay: document.querySelector('player-display'),
    //     playerList: document.querySelector('player-list')
    // };
    //
    //
    // // window control
    // components.windowHeader.addEventListener('close', detail => {
    //     chrome.app.window.current().close();
    // });
    //
    // components.windowHeader.addEventListener('minimize', detail => {
    //     chrome.app.window.current().minimize();
    // });
    //
    // components.windowHeader.addEventListener('showSettings', detail => {
    //     //show settings
    // });
    //
    //
    //
    // components.playerList.addEventListener('loaded', detail => {
    //     let {playlist} = detail;
    //     components.playerDisplay.setTrack({
    //         track: playlist[0],
    //         play: false
    //     });
    // });
    //
    //
    // components.playerList.addEventListener('choose', detail => {
    //     let {track} = detail;
    //     components.playerDisplay.setTrack({
    //         track: track,
    //         play: true
    //     });
    // });

	document.addEventListener("DOMContentLoaded", () => {
		scope.dispatcher = new scope.Dispatcher();

		// init stores
		new scope.WindowStore();
		new scope.SettingsButtonStore();
		new scope.SettingsStore();
		new scope.UserStore();
		new scope.PlayerStore();
		new scope.PlaylistStore();


		scope.actions.windowLaunch()
			.then(() => {
				scope.actions.fetchPlaylist()
			});
	});

	
})(window.app);
