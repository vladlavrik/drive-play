(scope => {
	'use strict';

	scope.actions = scope.actions || {};
	
	
	Object.assign(scope.actions, {


		/**
		 * settings show
		 * @public
         */
		settingsToggleShow () {

			scope.dispatcher.dispatch({
				actionName: 'SETTINGS_TOGGLE_SHOW'
			});
		}
		
		
	});

	

})(window.app = window.app || {});
