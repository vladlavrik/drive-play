(scope => {
	'use strict';

	scope.actions = scope.actions || {};
	
	
	Object.assign(scope.actions, {

		fetchPlaylist () {
			return scope.gApi.getPlaylist()
				.then(playList => {
					scope.dispatcher.dispatch({
						actionName: 'SET_PLAY_LIST',
						playList
					});
				}, err => {
					console.warn('Fetch playlist error:', err);
				});
		},

		setPlaylist (playList) {
			return scope.dispatcher.dispatch({
				actionName: 'SET_PLAY_LIST',
				playList
			});
		},


		playTrack (id) {
			return scope.dispatcher.dispatch({
				actionName: 'PLAY_TRACK'
			});
		},


		pause () {
			return scope.dispatcher.dispatch({
				actionName: 'PAUSE'
			});
		},


		play () {
			return scope.dispatcher.dispatch({
				actionName: 'PLAY'
			});
		},


		togglePlay () {
			return scope.dispatcher.dispatch({
				actionName: 'TOGGLE_PLAY'
			});
		},


		playNext () {
			return scope.dispatcher.dispatch({
				actionName: 'PLAY_NEXT'
			});
		},


		playPrev () {
			return scope.dispatcher.dispatch({
				actionName: 'PLAY_PREV'
			});
		},


		setVolume () {
			return scope.dispatcher.dispatch({
				actionName: 'SET_VOLUME'
			});
		},
		
		enableLoop () {
			return scope.dispatcher.dispatch({
				actionName: 'ENABLE_LOOP'
			});
		},


		disableLoop () {
			return scope.dispatcher.dispatch({
				actionName: 'DISABLE_LOOP'
			});
		},

		enableShuffle () {
			return scope.dispatcher.dispatch({
				actionName: 'ENABLE_SHUFFLE'
			});
		},


		disableShuffle() {
			return scope.dispatcher.dispatch({
				actionName: 'DISABLE_SHUFFLE'
			});
		},

		enableEqualizer () {
			return scope.dispatcher.dispatch({
				actionName: 'ENABLE_EQUALIZER'
			});
		},


		disableEqualizer () {
			return scope.dispatcher.dispatch({
				actionName: 'DISABLE_EQUALIZER'
			});
		},

		enableFrequencies () {
			return scope.dispatcher.dispatch({
				actionName: 'ENABLE_FREQUENCIES'
			});
		},


		disableFrequencies () {
			return scope.dispatcher.dispatch({
				actionName: 'DISABLE_FREQUENCIES'
			});
		}
		
	});

	

})(window.app = window.app || {});
