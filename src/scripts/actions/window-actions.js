(scope => {
	'use strict';

	scope.actions = scope.actions || {};
	
	
	Object.assign(scope.actions, {


		/**
		 * window launch
		 * @public
         */
		windowLaunch () {

			// get identity data
			let tokenPromise = scope.gApi.getToken();
			let userInfoPromise = scope.gApi.getUserInfo();

			return Promise.all([tokenPromise, userInfoPromise])
				.then(([token, {email: userEmail, id: userId}]) => {
					scope.dispatcher.dispatch({
						actionName: 'WINDOW_LAUNCH',
						userEmail,
						userId
					});
				}, err => {
					console.log('Window launch error:', err);
				});
		},


		/**
		 * window close
		 * @public
		 */
		windowClose () {
			return scope.dispatcher.dispatch({
				actionName: 'WINDOW_CLOSE'
			}).then(() => {
				chrome.app.window.current().close();
			}, err => {
				console.warn('Can not close window', err);
			});
		},


		windowMinimize () {
			return scope.dispatcher.dispatch({
				actionName: 'WINDOW_MINIMIZE'
			});
		},


		windowResize (width, height) {
			return scope.dispatcher.dispatch({
				actionName: 'WINDOW_RESIZE',
				width,
				height
			});
		},


		windowMove (left, right) {
			return scope.dispatcher.dispatch({
				actionName: 'WINDOW_MOVE',
				left,
				right
			});
		}
		
	});

	

})(window.app = window.app || {});
