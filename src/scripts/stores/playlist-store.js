(scope => {
	'use strict';

	class PlaylistStore extends scope.DefaultStore {
		constructor() {
			super();

			this.component = document.querySelector('ui-playlist');

			scope.dispatcher.register(({actionName, playList}) => {

				switch (actionName) {
					case 'SET_PLAY_LIST':
						this.setPlaylist(playList);
						break;


				}

			})
		}


		setPlaylist(playList) {
			this.component.setList(playList);
		}

	}

	scope.PlaylistStore = PlaylistStore;

})(window.app = window.app || {});
