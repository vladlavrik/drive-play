(scope => {
	'use strict';

	class SettingsButtonStore extends scope.DefaultStore {

		constructor() {
			super();

			this.component = document.querySelector('ui-settings-button');

			this.component.addEventListener('click', this.showSettings);

			scope.dispatcher.register(({actionName, userEmail}) => {
				switch (actionName) {
					case 'WINDOW_LAUNCH':
						this.setUserEmail(userEmail);
						break;
				}
			});
			
			
		}

		setUserEmail(email) {
			this.component.textContent = email;
		}
		
		showSettings() {
			scope.actions.settingsToggleShow();
		}
	}

	scope.SettingsButtonStore = SettingsButtonStore;

})(window.app = window.app || {});
