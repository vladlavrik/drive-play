(scope => {
	'use strict';

	class WindowStore extends scope.DefaultStore {
		constructor() {
			super();

			this.component = document.querySelector('ui-window');

			scope.dispatcher.register(({actionName}) => {

				switch (actionName) {
					case 'WINDOW_LAUNCH':
						break;
				}
			});

		}
	}

	scope.WindowStore = WindowStore;

})(window.app = window.app || {});
