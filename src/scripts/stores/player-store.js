(scope => {
	'use strict';

	class PlayerStore extends scope.DefaultStore {
		constructor() {
			super();
		}

	}

	scope.PlayerStore = PlayerStore;

})(window.app = window.app || {});
