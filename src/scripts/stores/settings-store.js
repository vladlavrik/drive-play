(scope => {
	'use strict';

	class SettingsStore extends scope.DefaultStore {

		constructor() {
			super();

			this.component = document.createElement('ui-settings');
			this.parent = document.querySelector('ui-window > header');

			scope.dispatcher.register(({actionName, userEmail}) => {

				switch (actionName) {
					case 'SETTINGS_TOGGLE_SHOW':

						this.toggleShow();
						break;
				}
			});
		}

		toggleShow() {
			return this.isShow
				? this.hide()
				: this.show()
		}

		show() {
			if (this.isShow) return false;
			this.parent.appendChild(this.component);
			this.isShow = true;
		}

		hide() {
			if (!this.isShow) return false;
			this.component.remove();
			this.isShow = false;
		}
	}

	scope.SettingsStore = SettingsStore;

})(window.app = window.app || {});
