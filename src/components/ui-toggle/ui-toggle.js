(() => {
    'use strict';
    let localDocument = document.currentScript.ownerDocument;
    let tpl = localDocument.querySelector('template').content;

    let elementProto = Object.create(HTMLElement.prototype);


	/**
	 * Change state of toggle button
	 * @param {Event} e
     */
	function toggleActive (e) {
		let event = new CustomEvent('change', {
			bubbles: false,
			cancelable: true,
			detail: {
				toActivate: !this.isActive
			}
		});

		let moveOn = this.dispatchEvent(event);

		if(!moveOn) {
			return;
		}

		if(this.isActive) {
			this.isActive = false;
			this.pointElement.classList.remove('active');
		} else {
			this.isActive = true;
			this.pointElement.classList.add('active');
		}
	}


	elementProto.createdCallback = function() {
		let root = this.createShadowRoot();
		let element = document.importNode(tpl, true);
		root.appendChild(element);

		this.pointElement = root.querySelector('.point');
    };

	elementProto.attachedCallback = function() {
		this.addEventListener('click', toggleActive, true);
    };

	elementProto.detachedCallback = function() {
		this.removeEventListener('click', toggleActive, true);
    };

    document.registerElement('ui-toggle', {
        prototype: elementProto
    });

})();
