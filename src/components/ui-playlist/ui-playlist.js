(() => {
	'use strict';
	let localDocument = document.currentScript.ownerDocument;
	let tpl = localDocument.querySelector('template').content;
	let tplItem = tpl.querySelector('.item');
	tpl.removeChild(tplItem);

	let elementProto = Object.create(HTMLElement.prototype);

	
	elementProto.createdCallback = function() {
		var root = this.createShadowRoot();
		let element = document.importNode(tpl, true);
		let mainSection = element.querySelector('.playlist');

		mainSection.style.width = this.getAttribute('css-width');
		mainSection.style.height = this.getAttribute('css-height');

		root.appendChild(element);
	};


	elementProto.attributeChangedCallback = function (name, old, value) {
		// update width
		let mainSection = this.shadowRoot.querySelector('.playlist');
		if(name === 'css-width') {
			mainSection.style.width = this.getAttribute('css-width');
		}
		// update height
		if(name === 'css-height') {
			mainSection.style.height = this.getAttribute('css-height');
		}
	};


	elementProto.setList = function (dataList) {
		let listElement = this.shadowRoot.querySelector('.list');
		let countElement = this.shadowRoot.querySelector('.list-count');
		let fragment = document.createDocumentFragment();

		dataList.forEach((item, num) => {
			let element = tplItem.cloneNode(true);
			let nameEl = element.querySelector('.name');
			let numberEl = element.querySelector('.number');
			nameEl.innerText = item.name;
			numberEl.innerText = num + 1;
			fragment.appendChild(element);
		});

		listElement.appendChild(fragment);
		countElement.innerText = dataList.length
	};


	document.registerElement('ui-playlist', {
		prototype: elementProto
	});

})();
