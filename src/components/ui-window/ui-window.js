(() => {
    'use strict';
    let localDocument = document.currentScript.ownerDocument;
    let tpl = localDocument.querySelector('template').content;

    let elementProto = Object.create(HTMLElement.prototype);

	elementProto.createdCallback = function() {
        var root = this.createShadowRoot();
        root.appendChild(document.importNode(tpl, true));
    };

    document.registerElement('ui-window', {
        prototype: elementProto
    });

})();
