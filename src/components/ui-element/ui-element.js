(() => {
    'use strict';
	/*
	 *
	 *
	 *
	 *
	 */

	let localDocument = document.currentScript.ownerDocument;

	// bubble element
	let bubbleEl = document.createElement('div');
	bubbleEl.classList.add('bubble');




	class UIElement extends HTMLElement {
		constructor(options) {
			this.template = options.template;
		}

		createdCallback () {
			let root = this.createShadowRoot();
			let element = document.importNode(this.template, true);
			root.appendChild(element);
		}


		attachedCallback() {
		}


		detachedCallback() {
		}


		attributeChangedCallback (nameAttr, old, value) {

		}


	}


	/**
	 * Append to DOM bubble element
	 * @param {Event} e
     */
	function launchBubble (e) {
		let target = e.currentTarget;
		let bubble = bubbleEl.cloneNode(false);
		let box = target.getBoundingClientRect();

		let width = target.clientWidth;
		let height = target.clientHeight;
		let size = Math.max(width, height) * 2;
		let top = e.clientY - box.top;
		let left = e.clientX - box.left;

		bubble.style.width = size + 'px';
		bubble.style.height = size + 'px';
		bubble.style.top = (top - size / 2) + 'px';
		bubble.style.left = (left- size / 2) + 'px';

		bubble.addEventListener('animationend', removeBubble);


		target.shadowRoot.appendChild(bubble);

	}


	/**
	 * Aemove from DOM bubble element after animation end
	 * @param {Event} e
	 */
	function removeBubble (e) {
		let bubble = e.currentTarget;
		bubble.removeEventListener('animationend', removeBubble);
		bubble.remove()
	}



	elementProto.createdCallback = function() {
		let root = this.createShadowRoot();
		let element = document.importNode(tpl, true);
		root.appendChild(element);

    };


	elementProto.attachedCallback = function() {
		this.addEventListener('click', launchBubble);
    };


	elementProto.detachedCallback = function() {
		this.removeEventListener('click', launchBubble);
    };

	
    document.registerElement('ui-button', {
        prototype: elementProto
    });

})();
