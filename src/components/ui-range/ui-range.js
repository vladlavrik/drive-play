(() => {
	'use strict';
	/*
	 *
	 *
	 *
	 *
	 */

	let localDocument = document.currentScript.ownerDocument;
	let tpl = localDocument.querySelector('template').content;
	let defaultProperties = {
		direction: 'horizontal',
		editable: false,
		bullet: false,
		finalOnly: false,
		padding: 0,
		min: 0,
		max: 10,
		step: 0,
		value: 0,
		shadowValue: 0
	};




	class Range extends HTMLElement {

		createdCallback () {
			this.bindThis('startChangeCallback', 'stepChangeCallback', 'stopChangeCallback');

			let root = this.createShadowRoot();
			let element = document.importNode(tpl, true);
			root.appendChild(element);

			this.rangeElement = root.querySelector('.range');
			this.valueElement = root.querySelector('.complete-value');
			this.shadowValueElement = root.querySelector('.complete-shadow');
			this.valueBulletElement = root.querySelector('.editable-bullet');

			// set initial properties
			for(let attrName of Object.keys(defaultProperties)) {
				let value = this.getAttribute(attrName);
				if (value) {
					this.attributeChangedCallback(attrName, null, value);
				} else {
					this[attrName] = defaultProperties[attrName];
				}
			}
		}


		attachedCallback() {
			if(this.editable) {
				this.addEventListener('mousedown', this.startChangeCallback);
			}
		}


		detachedCallback() {
			this.stopChangeCallback();
			this.addEventListener('mousedown', this.startChangeCallback);
		}


		attributeChangedCallback (nameAttr, old, value) {
			nameAttr = this.toCamelCase(nameAttr);

			switch (nameAttr) {

				case 'direction':
					this[nameAttr] = value;
					break;

				case 'editable':
				case 'bullet':
				case 'finalOnly':
					this[nameAttr] = value !== 'false';
					break;


				case 'padding':
					let valueInt = parseInt(value);
					if(!isNaN(valueInt)) {
						this[nameAttr] = valueInt;
					}
					break;

				case 'value':
				case 'shadowValue':
				case 'min':
				case 'max':
				case 'step':
					let valueFloat = parseFloat(value);
					if(!isNaN(valueFloat)) {
						this[nameAttr] = valueFloat;
					}
					break;
			}
		}


		startChangeCallback (event) {
			event.preventDefault();
			this.handleChange(event.clientX, event.clientY, false);

			this.changeProcess = true;
			document.addEventListener('mousemove', this.stepChangeCallback);
			document.addEventListener('mouseup', this.stopChangeCallback);
		}


		stepChangeCallback (event) {
			event.preventDefault();
			this.handleChange(event.clientX, event.clientY, false);
		}


		stopChangeCallback (event) {
			event && event.preventDefault();
			this.handleChange(event.clientX, event.clientY, true);

			this.changeProcess = false;
			document.removeEventListener('mousemove', this.stepChangeCallback);
			document.removeEventListener('mouseup', this.stopChangeCallback);
		}


		handleChange(posX, posY, isFinally) {
			let value = this.calculateValue(posX, posY);
			let canDispatch = isFinally || !this.finalOnly;
			if (canDispatch && value !== this.value) {
				this.value = value;
			}

			this.setUIValue(value);
		}


		calculateValue(posX, posY) {
			let {min, max, step} = this;
			let length = max - min;
			let {left: elemX, top: elemY} = this.getBoundingClientRect();
			let elemWidth = this.clientWidth;
			let elemHeight = this.clientHeight;

			let percent = this.direction === 'horizontal'
				? +((posX - elemX) / elemWidth)
				: -((posY - elemY) / elemHeight - 1);

			percent = Math.max(percent, 0);
			percent = Math.min(percent, 1);

			let value = percent * length + min;

			//TODO если шаг не кратен максимальном и минимальном значению

			return value;
		}


		setUIValue(value) {
			if(this.UIValue === value) {
				return;
			}

			let {min, max} = this;
			let length = max - min;
			let percent = Math.abs(value - min) / length * 100;

			let isHorizontal = this.direction === 'horizontal';
			let sizeProp = isHorizontal ? 'width' : 'height';
			let marginProp = isHorizontal ? 'left' : 'bottom';

			this.valueElement.style[sizeProp] = percent + '%';
			this.valueBulletElement.style[marginProp] = percent + '%';

			this.UIValue = value;
		}


		setUIValueShadow(value) {
			//todo
		}


		dispatchValue(value) {
			let event = new CustomEvent('change', {
				bubbles: false,
				cancelable: true,
				detail: {value}
			});

			return this.dispatchEvent(event);
		}


		toCamelCase(string) {
			return string.replace(/-([a-z])/gi, (p, g) => {
				return g.toUpperCase();
			})
		}


		bindThis(...methods) {
			for(let method of methods) {
				this[method] = this[method].bind(this);
			}
		}



		/*
		 * Element properties:
		 * -direction
		 * -editable
		 * -bullet
		 * -value
		 * -shadowValue
		 * -padding
		 * -min
		 * -max
		 * -step
		 * -finalOnly
		 */
		get direction() {
			return this.__direction;
		}
		set direction(value) {
			this.__direction = value === 'vertical'
				? 'vertical'
				: 'horizontal';
		}


		get editable() {
			return this.__editable;
		}
		set editable(value) {
			this.__editable = !!value;
		}


		get bullet() {
			return this.__bullet;
		}
		set bullet(value) {
			this.__bullet = !!value;
		}


		get value() {
			return this.__value;
		}
		set value(value) {
			let canApply = this.dispatchValue(value);
			if (canApply) {
				this.__value = value;
				this.setUIValue(value);
			}
		}


		get shadowValue() {
			return this.__shadowValue;
		}
		set shadowValue(value) {
			this.__shadowValue = value;
			this.setUIValueShadow(value);
		}


		get padding() {
			return this.__padding;
		}
		set padding(value) {
			this.__padding = value;
		}


		get min() {
			return this.__min;
		}
		set min(value) {
			this.__min = value;
		}

		get max() {
			return this.__max;
		}
		set max(value) {
			this.__max = value;
		}


		get step() {
			return this.__step;
		}
		set step(value) {
			this.__step = value;
		}


		get finalOnly() {
			return this.__finalOnly;
		}
		set finalOnly(value) {
			this.__finalOnly = !!value;
		}

	}




	document.registerElement('ui-range', {
		prototype: Range.prototype
	});


})();




//TODO: touchscreen support (touch events), only left mouse key, set and control separator

/*
при начале изминений если только финошное применение ставим флаг lockValue а при назначении валуе если лок игнорим



 */
