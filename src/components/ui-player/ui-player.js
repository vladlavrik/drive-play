(() => {
	'use strict';
	let localDocument = document.currentScript.ownerDocument;
	let tpl = localDocument.querySelector('template').content;

	let elementProto = Object.create(HTMLElement.prototype);


	elementProto.createdCallback = function() {
		var root = this.createShadowRoot();
		let element = document.importNode(tpl, true);
		root.appendChild(element);
	};


	elementProto.attributeChangedCallback = function (name, old, value) {

	};



	document.registerElement('ui-player', {
		prototype: elementProto
	});

})();
