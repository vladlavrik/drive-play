(() => {
    'use strict';
    // let localDocument = document.currentScript.ownerDocument;
    // let tpl = localDocument.querySelector('template').content;

    let elementProto = Object.create(HTMLElement.prototype);

	elementProto.createdCallback = function() {
		let key = this.getAttribute('string');
		this.innerHTML = chrome.i18n.getMessage(key) || newValue;
    };

	elementProto.attributeChangedCallback = function (attrName, oldValue, newValue) {
		if(attrName === 'string') {
			this.innerHTML = chrome.i18n.getMessage(newValue) || newValue;
		}
	};


    document.registerElement('ui-i18n', {
        prototype: elementProto
    });

})();
